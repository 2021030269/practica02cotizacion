package Controlador;

import Modelo.Cotizacion;
import Vista.dlgCotizacion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {

    private Cotizacion mod;
    private dlgCotizacion vista;

    public Controlador(Cotizacion mod, dlgCotizacion vista) {
        this.mod = mod;
        this.vista = vista;

        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);

        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
    }

    private void iniciarVista(){
        vista.setTitle(":: Cotizacion ::");
        vista.setSize(1070, 780);
        vista.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            limpiar(); 
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.btnCerrar.setEnabled(true);
            
            vista.txtNumCotiz.setEnabled(true);
            vista.txtDescrip.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.txtPorcenPagIni.setEnabled(true);
            vista.cboPlazo.setEnabled(true);
        }
        else if(e.getSource() == vista.btnGuardar){
            boolean Ex = false;
            if(ValiEsp() == true){
                
                try{
                    mod.setNumCotiz(Integer.parseInt(vista.txtNumCotiz.getText()));
                    mod.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                    mod.setPlazo(Float.parseFloat(vista.cboPlazo.getSelectedItem().toString()));
                    mod.setPorcenPagIni(Float.parseFloat(vista.txtPorcenPagIni.getText()));
                    Ex = true;
                }
                catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex);
                    
                }
                catch(Exception ex2){
                    JOptionPane.showMessageDialog(vista,"Ocurrio el error: "+ex2);
                }
                if(Ex != false){
                    
                    mod.setDescrip(vista.txtDescrip.getText());
                    JOptionPane.showMessageDialog(vista,"Se a guardado con exito");
                    limpiar(); 
                }
                  
            }
            else{
                JOptionPane.showMessageDialog(vista,"No deje ningun espacio en blanco");
            }
        }
        else if(e.getSource()==vista.btnMostrar){
            vista.txtNumCotiz.setEnabled(false);
            vista.txtDescrip.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.txtPorcenPagIni.setEnabled(false);
            vista.cboPlazo.setEnabled(false);
            
            vista.txtNumCotiz.setText(String.valueOf(mod.getNumCotiz()));
            vista.txtDescrip.setText(mod.getDescrip());
            vista.txtPrecio.setText(String.valueOf(mod.getPrecio()));
            vista.txtPorcenPagIni.setText(String.valueOf(mod.getPorcenPagIni()));
            vista.cboPlazo.setSelectedItem(String.valueOf(mod.getPlazo()));
            vista.txtPagoIni.setText(String.valueOf(mod.pagoInicial()));
            vista.txtTotalFin.setText(String.valueOf(mod.TotalFin()));
            vista.txtPagoMens.setText(String.valueOf(mod.PagoMensual()));
       
        }
        else if(e.getSource()==vista.btnCancelar){
            limpiar();
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
            vista.btnCerrar.setEnabled(false);
            
            vista.txtNumCotiz.setEnabled(false);
            vista.txtDescrip.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.txtPorcenPagIni.setEnabled(false);
            vista.cboPlazo.setEnabled(false);
        }
        else if(e.getSource()==vista.btnCerrar){
            if(JOptionPane.showConfirmDialog(vista,"Seguro que desea cerrar?","Cerrar",JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION){
                
            }
            else{
                vista.setVisible(false);
                vista.dispose();
                System.exit(0);
            }   
        }
        else if(e.getSource()==vista.btnLimpiar){
            limpiar();
        }

    }
    public void limpiar(){
        vista.txtNumCotiz.setText("");
        vista.txtDescrip.setText("");
        vista.txtPrecio.setText("");
        vista.txtPorcenPagIni.setText("");
        vista.cboPlazo.setSelectedItem(" ");
        vista.txtPagoIni.setText("");
        vista.txtTotalFin.setText("");
        vista.txtPagoMens.setText("");
        
    }
    public boolean ValiEsp(){
        if(!vista.txtNumCotiz.getText().isEmpty()
            && !vista.txtDescrip.getText().isEmpty()
            && !vista.txtPrecio.getText().isEmpty()
            && !vista.txtPorcenPagIni.getText().isEmpty()
            && !vista.cboPlazo.getSelectedItem().toString().isEmpty()){
            return true;
        
        }
        else{
            return false;
        }
    }

    public static void main(String[] args) {
        Cotizacion mod = new Cotizacion();
        dlgCotizacion vista = new dlgCotizacion();
        Controlador Contro = new Controlador(mod, vista);
        Contro.iniciarVista();
        
    }
}
