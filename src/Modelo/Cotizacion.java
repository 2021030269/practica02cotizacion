
package Modelo;

public class Cotizacion {
    private int numCotiz;
    private String descrip;
    private float precio = 0.2f,porcenPagIni = 0.2f,plazo = 0.2f;

    public Cotizacion() {
    }

    public Cotizacion(int numCotiz, String descrip, float precio, float porcenPagIni, float plazo) {
        this.numCotiz = numCotiz;
        this.descrip = descrip;
        this.precio = precio;
        this.porcenPagIni = porcenPagIni;
        this.plazo = plazo;
    }
    
    public Cotizacion(Cotizacion cotiza){
        
    }

    public void setNumCotiz(int numCotiz) {
        this.numCotiz = numCotiz;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public void setPorcenPagIni(float porcenPagIni) {
        this.porcenPagIni = porcenPagIni;
    }

    public void setPlazo(float plazo) {
        this.plazo = plazo;
    }

    public int getNumCotiz() {
        return numCotiz;
    }

    public String getDescrip() {
        return descrip;
    }

    public float getPrecio() {
        return precio;
    }

    public float getPorcenPagIni() {
        return porcenPagIni;
    }

    public float getPlazo() {
        return plazo;
    }
    
    public float pagoInicial(){
        return getPrecio()*(getPorcenPagIni()/100);
    }
    
    public float TotalFin(){
        return getPrecio()-pagoInicial();
    }
    
    public float PagoMensual(){
        return TotalFin()/getPlazo();
    }
    
}
